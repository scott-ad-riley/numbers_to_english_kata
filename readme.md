reads from stdin and prints out the english representation of the number:

For example:

* `1` becomes `one`

* `999999999` becomes `nine hundred and ninety-nine million nine hundred and ninety-nine thousand nine hundred and ninety-nine`

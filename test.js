const numberToEnglish = require('./numbers_to_english')

function createTest([v1, v2]) {
  it(`works for ${v1}`, function () {
    expect(numberToEnglish(v1)).toEqual(v2)
  })
}

const testCases = [
  [0, 'zero'],
  [1, 'one'],
  [2, 'two'],
  [3, 'three'],
  [4, 'four'],
  [5, 'five'],
  [6, 'six'],
  [7, 'seven'],
  [8, 'eight'],
  [9, 'nine'],
  [10, 'ten'],
  [11, 'eleven'],
  [30, 'thirty'],
  [99, 'ninety-nine'],
  [100, 'one hundred'],
  [123, 'one hundred and twenty-three'],
  [207, 'two hundred and seven'],
  [1000, 'one thousand'],
  [1001, 'one thousand and one'],
  [2001, 'two thousand and one'],
  [3100, 'three thousand one hundred'],
  [3101, 'three thousand one hundred and one'],
  [4110, 'four thousand one hundred and ten'],
  [5111, 'five thousand one hundred and eleven'],
  [19000, 'nineteen thousand'],
  [319000, 'three hundred and nineteen thousand'],
  [1000000, 'one million'],
  [1000001, 'one million and one'],
  [1011011, 'one million eleven thousand and eleven'],
  [1000001, 'one million and one'],
  [1000011, 'one million and eleven'],
  [1000111, 'one million one hundred and eleven'],
  [1011011, 'one million eleven thousand and eleven'],
  [100000000, 'one hundred million'],
  [999000999, 'nine hundred and ninety-nine million nine hundred and ninety-nine'],
  [999001999, 'nine hundred and ninety-nine million one thousand nine hundred and ninety-nine'],
  [999999999, 'nine hundred and ninety-nine million nine hundred and ninety-nine thousand nine hundred and ninety-nine'],
  [999000000000, 'nine hundred and ninety-nine billion'],
  [999999000000, 'nine hundred and ninety-nine billion nine hundred and ninety-nine million'],
  [999999999000, 'nine hundred and ninety-nine billion nine hundred and ninety-nine million nine hundred and ninety-nine thousand'],
  [999999999900, 'nine hundred and ninety-nine billion nine hundred and ninety-nine million nine hundred and ninety-nine thousand nine hundred'],
  [999999999099, 'nine hundred and ninety-nine billion nine hundred and ninety-nine million nine hundred and ninety-nine thousand and ninety-nine'],
  [999999999999, 'nine hundred and ninety-nine billion nine hundred and ninety-nine million nine hundred and ninety-nine thousand nine hundred and ninety-nine']
]

testCases.forEach(createTest)

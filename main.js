const numbersToWords = require('./numbers_to_english')
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('What do you think of Node.js? ', (answer) => {
  console.log(`Your number is: ${numbersToWords(answer)}`);

  rl.close();
});

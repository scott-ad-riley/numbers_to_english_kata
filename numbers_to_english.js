function numberToEnglish(n) {
  const small = []
  small[0] = 'zero'
  small[1] = 'one'
  small[2] = 'two'
  small[3] = 'three'
  small[4] = 'four'
  small[5] = 'five'
  small[6] = 'six'
  small[7] = 'seven'
  small[8] = 'eight'
  small[9] = 'nine'
  small[10] = 'ten'
  small[11] = 'eleven'
  small[12] = 'twelve'
  small[13] = 'thirteen'
  small[14] = 'fourteen'
  small[15] = 'fifteen'
  small[16] = 'sixteen'
  small[17] = 'seventeen'
  small[18] = 'eighteen'
  small[19] = 'nineteen'
  small[20] = 'twenty'

  const medium = []
  medium[1] = 'ten'
  medium[2] = 'twenty'
  medium[3] = 'thirty'
  medium[4] = 'forty'
  medium[5] = 'fifty'
  medium[5] = 'fifty'
  medium[6] = 'sixty'
  medium[7] = 'seventy'
  medium[8] = 'eighty'
  medium[9] = 'ninety'

  function digit(d, x) {
    const s = String(x)
    return Number(s.slice(s.length - d, s.length - (d - 1)))
  }
  function digitRange(first, last, x) {
    const s = String(x)
    return Number(s.slice(s.length - first, s.length - last))
  }
  function emptyAt(positions, x) {
    if (typeof positions !== 'object') positions = [positions]
    const result = positions.find((item) => digit(item, x) === 0)
    return result !== undefined
  }
  function emptyRange(positions, x) {
    return !positions.map((position) => emptyAt(position, x)).includes(false)
  }
  function handleSingle(x) {
    return small[digit(1, x)]
  }
  function handleTens(x) {
    if (digitRange(2, 0, x) <= 20) return small[digitRange(2, 0, x)]
    let result = medium[digit(2, x)]
    if (!emptyAt(1, x)) result = `${result}-${small[digit(1, x)]}`
    return result
  }
  function handleHundreds(x) {
    if (x <= 20) return small[x]
    if (emptyAt(3, x)) return handleTens(x)
    let result = `${small[digit(3, x)]} hundred`
    if (!emptyAt(2, x) || !emptyAt(1, x)) return `${result} and ${handleTens(x)}`
    return result
  }
  function handleThousands(x) {
    const thousandsCount = digitRange(6, 3, x) || digitRange(5, 3, x) || digitRange(4, 3, x)
    let result = `${handleHundreds(thousandsCount)} thousand`
    if (thousandsCount === 0) return handleHundreds(x)
    if (emptyRange([3, 2, 1], x)) return result
    if (emptyAt(3, x)) return `${result} and ${handleHundreds(x)}`
    return `${result} ${handleHundreds(x)}`
  }
  function handleMillions(x) {
    const millionsCount = digitRange(9, 6, x) || digitRange(8, 6, x) || digitRange(7, 6, x)
    let result = `${handleHundreds(millionsCount)} million`
    if (emptyRange([6, 5, 4, 3, 2, 1], x)) return result
    if (emptyRange([6, 5, 4, 3], x)) return `${result} and ${handleHundreds(x)}`
    return `${result} ${handleThousands(x)}`
  }
  function handleBillions(x) {
    const billionsCount = digitRange(12, 9, x) || digitRange(11, 9, x) || digitRange(10, 9, x)
    let result = `${handleHundreds(billionsCount)} billion`
    if (emptyRange([9, 8, 7, 6, 5, 4, 3, 2, 1], x)) return result
    return `${result} ${handleMillions(x)}`
  }
  if (n <= 20) return small[n];
  if (n <= 99) return handleTens(n);
  if (n <= 999) return handleHundreds(n);
  if (n <= 999999) return handleThousands(n);
  if (n <= 999999999) return handleMillions(n);
  if (n <= 999999999999) return handleBillions(n);
  return "not implemented";
}

module.exports = numberToEnglish;
